import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Open classroom angular exercie 1';

  posts = [

    {

      title: 'Mon premier post',

      content: 'Ceci est le content de mon premier post.',

      loveIts:0,

    },

    {

      title: 'Mon deuxième post',

      content: 'Pour ce deuxième post voici le content.',
      
      loveIts:0,

    },

    {

       title: 'Encore un post',

      content: 'Pour un autre post il y a bien un autre content, xd!!',
      
      loveIts:0,

    }

  ];
}
