import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

 @Input() title: string;
	@Input() content: string;
	@Input() loveIts: number;
	created_at = new Promise((resolve, reject) => {

	    const date = new Date();

	    setTimeout(

	      () => {

	        resolve(date);

	      }

	    );

	  });

	constructor() { }

	ngOnInit() { }

	 getColor()
 	{

	    if(this.loveIts == 0)
	    {

	      return 'black';

	    } 
	    else if(this.loveIts > 0)
	    {

	      return 'blue';
	    }

	    else 
	    {

	      return 'red';
	    }

 	}
		
	loveit()
	{
	   this.loveIts++;
    }

	dontloveit()
	{

	   this.loveIts--;

	}

}
